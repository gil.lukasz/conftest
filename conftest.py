import pytest
from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
import os


@pytest.fixture()
def local_setup(request):
    options = Options()
    options.page_load_strategy = 'normal'
    chrome_options = webdriver.ChromeOptions()

    if os.name == 'nt':
        driver = webdriver.Chrome(ChromeDriverManager().install(), options=chrome_options)
        driver.maximize_window()
        request.cls.driver = driver
    else:
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        # chrome_options.add_argument('start-maximized')
        # chrome_options.add_argument("window-size=1920,1080")
        driver = webdriver.Chrome(options=chrome_options)
        request.cls.driver = driver
    yield
    driver.quit()
