from datetime import datetime

import allure
import pytest
from allure_commons.types import AttachmentType
from selenium.webdriver import ActionChains as AC
from selenium.webdriver.common.by import By

from Open_tvn24 import Open_and_rodo_accept as Open


@pytest.mark.usefixtures("local_setup")
@allure.title("Tvn24 - Test obecnosci video w reportażach")
class Test_Articles:

    def test_button_Articles_Video_Presence(self):

        Open_method = Open(self.driver)
        Open_method.open_tvn24_page()
        Open_method.onetrust_rodo_accept()
        Open_method.tvn24_load_page_check()

        self.date_hour = datetime.now().strftime("%d-%m-%Y %H_%M_%S.%f")
        self.CSS_header = "li.header__list-item--menu"
        self.CSS_header_categories_visible = "header .header-menu__row-list>li"

        self.category_button_class = "header-menu__item"
        self.category_roll_button_class = "header-menu__item header-menu__item--with-submenu"

        self.hidden_button_class_name = "not (contains(@class, 'header-menu__item--hidden'))"
        self.more_button_xpath = f".//header//button[@class='header-menu__more-button']"
        driver = self.driver
        category = "NAJNOWSZE"
        header = driver.find_element(By.CSS_SELECTOR, self.CSS_header)
        driver.execute_script("arguments[0].scrollIntoView(true);", header)

        menu_categories_visible = driver.find_elements(By.CSS_SELECTOR, self.CSS_header_categories_visible)
        print("found: ", len(menu_categories_visible))
        for category_button in menu_categories_visible:
            category_button_class = category_button.get_attribute("class")
            if category_button_class == self.category_button_class and category_button_class != self.category_roll_button_class:
                category_button_name = category_button.find_element_by_tag_name("a")
                if category_button_name.get_attribute("innerText") == category:
                    print(f"User click on {category} button")
                    AC(driver).move_to_element(category_button_name).perform()
                    allure.attach(self.driver.get_screenshot_as_png(),
                                  name=f"User click on {category} button - {self.date_hour}",
                                  attachment_type=AttachmentType.PNG)
                    category_button.click()
                else:
                    continue
            else:
                continue


